# Battle.NET [![Build status](https://ci.appveyor.com/api/projects/status/gv56wug279wj5ht6?svg=true)](https://ci.appveyor.com/project/nick-strohm/battlenet) [![NuGet](https://img.shields.io/nuget/v/Battle.NET.svg)]() [![Travis](https://img.shields.io/badge/license-MIT%20License-blue.svg)]()
Welcome to Battle.NET. A simple C-Sharp library to access the battle.net-API

## From NuGet
Battle.NET is available from NuGet. Just type at your package manager console ``Install-Package Battle.NET`` and it will install Battle.NET and its depencies (Newtonsoft.JSON)

## How to use it?
Simply follow the example below:
```c#
BattleNETClient client = new BattleNETClient("apikey", "apisecret", BattleNETClient.BattleNetRegion.EU, BattleNETClient.BattleNetLocale.de_DE);

client.Connected += (sender, e) =>
{
    Console.WriteLine($"Connected to region {e.region}!");
    Console.WriteLine($"Tested URL ({e.url}) answered with [{e.status}]");
    Console.WriteLine("");
    
    Objects.WOW.Achievement achievement = client.WOW.getAchievement(2144); // PARAM -> Achievement ID
    Console.WriteLine($"{achievement.id}: {achievement.title}");
};

// Then you can check, if your credentials are right
client.CredentialCheck(); // -> Fires the Connected event
// client.CredentialCheckAsync(); // -> Fires the Connected event
```

## Compatible API-Endpoints
[Click me](API-Endpoints.md)

## License
Copyright (c) 2016 [Nick Strohm](https://gitlab.com/nick-strohm/)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Copyright notices
*Battle.net, Diablo, World of Warcraft and Warcraft are trademarks or registered trademarks of Blizzard Entertainment, Inc., in the U.S. and/or other countries.*