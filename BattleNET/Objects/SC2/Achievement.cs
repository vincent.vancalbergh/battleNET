﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.SC2
{
    public class Achievement
    {
        public string title { get; internal set; }
        public string description { get; internal set; }
        public long achievementId { get; internal set; }
        public int categoryId { get; internal set; }
        public int points { get; internal set; }
        public Icon icon { get; internal set; }

        public Achievement(JObject rawJson)
        {
            this.title = rawJson["title"].ToString();
            this.description = rawJson["description"].ToString();
            this.achievementId = long.Parse(rawJson["achievementId"].ToString());
            this.categoryId = int.Parse(rawJson["categoryId"].ToString());
            this.points = int.Parse(rawJson["points"].ToString());
            this.icon = new Icon(JObject.Parse(rawJson["icon"].ToString()));
        }
    }
}
