﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.D3
{
    public class Heroe
    {
        public class HeroeKills
        {
            public int monsters { get; internal set; }
            public int elites { get; internal set; }
            public int hardcoreMonsters { get; internal set; }

            public HeroeKills(JObject JKills)
            {
                this.monsters = int.Parse(JKills["monsters"].ToString());
                this.elites = int.Parse(JKills["elites"].ToString());
                this.hardcoreMonsters = int.Parse(JKills["hardcoreMonsters"].ToString());
            }
        }
        public class HeroeSkills
        {
            public class HeroeSkillsItem
            {
                public class HeroeSkillsItemSubitem
                {
                    public string slug { get; internal set; }
                    public string name { get; internal set; }
                    public string icon { get; internal set; }
                    public int level { get; internal set; }
                    public string categorySlug { get; internal set; }
                    public string tooltipUrl { get; internal set; }
                    public string description { get; internal set; }
                    public string simpleDescription { get; internal set; }
                    public string skillCalcId { get; internal set; }

                    public HeroeSkillsItemSubitem(JObject JSkillsItemSubitem)
                    {
                        this.slug = JSkillsItemSubitem["slug"].ToString();
                        this.name = JSkillsItemSubitem["name"].ToString();
                        this.icon = JSkillsItemSubitem["icon"].ToString();
                        this.level = int.Parse(JSkillsItemSubitem["level"].ToString());
                        this.categorySlug = JSkillsItemSubitem["categorySlug"].ToString();
                        this.tooltipUrl = JSkillsItemSubitem["tooltipUrl"].ToString();
                        this.description = JSkillsItemSubitem["description"].ToString();
                        this.simpleDescription = JSkillsItemSubitem["simpleDescription"].ToString();
                        this.skillCalcId = JSkillsItemSubitem["skillCalcId"].ToString();
                    }
                }

                public HeroeSkillsItemSubitem skill { get; internal set; }
                public HeroeSkillsItemSubitem rune { get; internal set; }

                public HeroeSkillsItem(JObject JSkillsItem)
                {
                    this.skill = new HeroeSkillsItemSubitem(JObject.Parse(JSkillsItem["skill"].ToString()));
                    this.rune = new HeroeSkillsItemSubitem(JObject.Parse(JSkillsItem["rune"].ToString()));
                }
            }

            public List<HeroeSkillsItem> active { get; internal set; }
            public List<HeroeSkillsItem> passive { get; internal set; }

            public HeroeSkills(JObject JSkills)
            {
                this.active = new List<HeroeSkillsItem>();
                foreach (JObject JSkill in JSkills["active"])
                    this.active.Add(new HeroeSkillsItem(JSkill));
                this.passive = new List<HeroeSkillsItem>();
                foreach (JObject JSkill in JSkills["passive"])
                    this.passive.Add(new HeroeSkillsItem(JSkill));
            }
        }
        public class HeroeItems
        {
            public Item head { get; internal set; }
            public Item torso { get; internal set; }
            public Item feet { get; internal set; }
            public Item hands { get; internal set; }
            public Item shoulders { get; internal set; }
            public Item legs { get; internal set; }
            public Item bracers { get; internal set; }
            public Item mainHand { get; internal set; }
            public Item offHand { get; internal set; }
            public Item waist { get; internal set; }
            public Item rightFinger { get; internal set; }
            public Item leftFinger { get; internal set; }
            public Item neck { get; internal set; }

            public HeroeItems(JObject JItems)
            {
                this.head = new Item(JObject.Parse(JItems["head"].ToString()));
                this.torso = new Item(JObject.Parse(JItems["torso"].ToString()));
                this.feet = new Item(JObject.Parse(JItems["feet"].ToString()));
                this.hands = new Item(JObject.Parse(JItems["hands"].ToString()));
                this.shoulders = new Item(JObject.Parse(JItems["shoulders"].ToString()));
                this.legs = new Item(JObject.Parse(JItems["legs"].ToString()));
                this.bracers = new Item(JObject.Parse(JItems["bracers"].ToString()));
                this.mainHand = new Item(JObject.Parse(JItems["mainHand"].ToString()));
                this.offHand = new Item(JObject.Parse(JItems["offHand"].ToString()));
                this.waist = new Item(JObject.Parse(JItems["waist"].ToString()));
                this.rightFinger = new Item(JObject.Parse(JItems["rightFinger"].ToString()));
                this.leftFinger = new Item(JObject.Parse(JItems["leftFinger"].ToString()));
                this.neck = new Item(JObject.Parse(JItems["neck"].ToString()));
            }
        }
        public class HeroeFollowers
        {
            public Heroe templar;
            public Heroe scoundrel;
            public Heroe enchantress;

            public HeroeFollowers(JObject JFollowers)
            {
                this.templar = new Heroe(JObject.Parse(JFollowers["templar"].ToString()));
                this.scoundrel = new Heroe(JObject.Parse(JFollowers["scoundrel"].ToString()));
                this.enchantress = new Heroe(JObject.Parse(JFollowers["enchantress"].ToString()));
            }
        }
        public class HeroeLegendaryPower
        {
            public string id { get; internal set; }
            public string name { get; internal set; }
            public string icon { get; internal set; }
            public string displayColor { get; internal set; }
            public string tooltipParams { get; internal set; }

            public HeroeLegendaryPower(JObject JLegendaryPower)
            {
                this.id = JLegendaryPower["id"].ToString();
                this.name = JLegendaryPower["name"].ToString();
                this.icon = JLegendaryPower["icon"].ToString();
                this.displayColor = JLegendaryPower["displayColor"].ToString();
                this.tooltipParams = JLegendaryPower["tooltipParams"].ToString();
            }
        }
        public class HeroeStats
        {
            public int life { get; internal set; }
            public float damage { get; internal set; }
            public float toughness { get; internal set; }
            public float healing { get; internal set; }
            public float attackSpeed { get; internal set; }
            public int armor { get; internal set; }
            public int strength { get; internal set; }
            public int dexterity { get; internal set; }
            public int vitality { get; internal set; }
            public int intelligence { get; internal set; }
            public int physicalResist { get; internal set; }
            public int fireResist { get; internal set; }
            public int coldResist { get; internal set; }
            public int lightningResist { get; internal set; }
            public int poisonResist { get; internal set; }
            public int arcaneResist { get; internal set; }
            public float critDamage { get; internal set; }
            public float blockChance { get; internal set; }
            public int blockAmountMin { get; internal set; }
            public int blockAmountMax { get; internal set; }
            public float damageIncrease { get; internal set; }
            public float critChance { get; internal set; }
            public float damageReduction { get; internal set; }
            public float thorns { get; internal set; }
            public float lifeSteal { get; internal set; }
            public float lifePerKill { get; internal set; }
            public float goldFind { get; internal set; }
            public float magicFind { get; internal set; }
            public float lifeOnHit { get; internal set; }
            public int primaryResource { get; internal set; }
            public int secondaryResource { get; internal set; }

            public HeroeStats(JObject JStats)
            {
                this.life = int.Parse(JStats["life"].ToString());
                this.damage = float.Parse(JStats["damage"].ToString());
                this.toughness = float.Parse(JStats["toughness"].ToString());
                this.healing = float.Parse(JStats["healing"].ToString());
                this.attackSpeed = float.Parse(JStats["attackSpeed"].ToString());
                this.armor = int.Parse(JStats["armor"].ToString());
                this.strength = int.Parse(JStats["strength"].ToString());
                this.dexterity = int.Parse(JStats["dexterity"].ToString());
                this.vitality = int.Parse(JStats["vitality"].ToString());
                this.intelligence = int.Parse(JStats["intelligence"].ToString());
                this.physicalResist = int.Parse(JStats["physicalResist"].ToString());
                this.fireResist = int.Parse(JStats["fireResist"].ToString());
                this.coldResist = int.Parse(JStats["coldResist"].ToString());
                this.lightningResist = int.Parse(JStats["lightningResist"].ToString());
                this.poisonResist = int.Parse(JStats["poisonResist"].ToString());
                this.arcaneResist = int.Parse(JStats["arcaneResist"].ToString());
                this.critDamage = float.Parse(JStats["critDamage"].ToString());
                this.blockChance = float.Parse(JStats["blockChance"].ToString());
                this.blockAmountMin = int.Parse(JStats["blockAmountMin"].ToString());
                this.blockAmountMax = int.Parse(JStats["blockAmountMax"].ToString());
                this.damageIncrease = float.Parse(JStats["damageIncrease"].ToString());
                this.critChance = float.Parse(JStats["critChance"].ToString());
                this.damageReduction = float.Parse(JStats["damageReduction"].ToString());
                this.thorns = float.Parse(JStats["thorns"].ToString());
                this.lifeSteal = float.Parse(JStats["lifeSteal"].ToString());
                this.lifePerKill = float.Parse(JStats["lifePerKill"].ToString());
                this.goldFind = float.Parse(JStats["goldFind"].ToString());
                this.magicFind = float.Parse(JStats["magicFind"].ToString());
                this.lifeOnHit = float.Parse(JStats["lifeOnHit"].ToString());
                this.primaryResource = int.Parse(JStats["primaryResource"].ToString());
                this.secondaryResource = int.Parse(JStats["secondaryResource"].ToString());
            }
        }
        public class HeroeProgression
        {
            public class HeroeProgressionAct
            {
                public class HeroeProgressionActCompletedQuest
                {
                    public string slug { get; internal set; }
                    public string name { get; internal set; }

                    public HeroeProgressionActCompletedQuest(JObject JProgressionActCompletedQuest)
                    {
                        this.slug = JProgressionActCompletedQuest["slug"].ToString();
                        this.name = JProgressionActCompletedQuest["name"].ToString();
                    }
                }

                public bool completed { get; internal set; }
                public List<HeroeProgressionActCompletedQuest> completedQuests { get; internal set; }

                public HeroeProgressionAct(JObject JProgressionAct)
                {
                    this.completed = bool.Parse(JProgressionAct["completed"].ToString());
                    this.completedQuests = new List<HeroeProgressionActCompletedQuest>();
                    foreach (JObject JCompletedQuest in JProgressionAct["completedQuests"])
                        this.completedQuests.Add(new HeroeProgressionActCompletedQuest(JCompletedQuest));
                }
            }

            public HeroeProgressionAct act1 { get; internal set; }
            public HeroeProgressionAct act2 { get; internal set; }
            public HeroeProgressionAct act3 { get; internal set; }
            public HeroeProgressionAct act4 { get; internal set; }
            public HeroeProgressionAct act5 { get; internal set; }

            public HeroeProgression(JObject JProgression)
            {
                this.act1 = new HeroeProgressionAct(JObject.Parse(JProgression["act1"].ToString()));
                this.act2 = new HeroeProgressionAct(JObject.Parse(JProgression["act2"].ToString()));
                this.act3 = new HeroeProgressionAct(JObject.Parse(JProgression["act3"].ToString()));
                this.act4 = new HeroeProgressionAct(JObject.Parse(JProgression["act4"].ToString()));
                this.act5 = new HeroeProgressionAct(JObject.Parse(JProgression["act5"].ToString()));
            }
        }

        public int id { get; internal set; }
        public string name { get; internal set; }
        [JsonProperty("class")]
        public string classId { get; internal set; }
        public int gender { get; internal set; }
        public int level { get; internal set; }
        public HeroeKills kills { get; internal set; }
        public int paragonLevel { get; internal set; }
        public bool hardcore { get; internal set; }
        public bool seasonal { get; internal set; }
        public int seasonCreated { get; internal set; }
        [JsonProperty("last-updated")]
        public long lastUpdated { get; internal set; }
        public bool dead { get; internal set; }
        public HeroeSkills skills { get; internal set; }
        public HeroeItems items { get; internal set; }
        public HeroeFollowers followers { get; internal set; }
        public List<HeroeLegendaryPower> legendaryPowers { get; internal set; }
        public HeroeStats stats { get; internal set; }
        public HeroeProgression progression { get; internal set; }

        public Heroe(JObject rawJson)
        {
            this.id = int.Parse(rawJson["id"].ToString());
            this.name = rawJson["name"].ToString();
            this.classId = rawJson["class"].ToString();
            this.gender = int.Parse(rawJson["gender"].ToString());
            this.level = int.Parse(rawJson["level"].ToString());
            this.kills = new HeroeKills(JObject.Parse(rawJson["kills"].ToString()));
            this.paragonLevel = int.Parse(rawJson["paragonLevel"].ToString());
            this.hardcore = bool.Parse(rawJson["hardcore"].ToString());
            this.seasonal = bool.Parse(rawJson["seasonal"].ToString());
            this.seasonCreated = int.Parse(rawJson["seasonCreated"].ToString());
            this.lastUpdated = long.Parse(rawJson["last-updated"].ToString());
            this.dead = bool.Parse(rawJson["dead"].ToString());
            this.skills = new HeroeSkills(JObject.Parse(rawJson["skills"].ToString()));
            this.items = new HeroeItems(JObject.Parse(rawJson["items"].ToString()));
            this.followers = new HeroeFollowers(JObject.Parse(rawJson["followers"].ToString()));
            this.legendaryPowers = new List<HeroeLegendaryPower>();
            foreach (JObject JLegendaryPower in rawJson["legendaryPowers"])
                this.legendaryPowers.Add(new HeroeLegendaryPower(JLegendaryPower));
            this.stats = new HeroeStats(JObject.Parse(rawJson["stats"].ToString()));
            this.progression = new HeroeProgression(JObject.Parse(rawJson["progression"].ToString()));
        }
    }
}
