﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleNET.Objects.WOW
{
    public class Realm
    {
        public class RealmZone
        {
            public int area { get; internal set; }
            [JsonProperty("controlling-faction")]
            public int controllingfaction { get; internal set; }
            public int status { get; internal set; }
            public long next { get; internal set; }

            public RealmZone(JObject JZone)
            {
                this.area = int.Parse(JZone["area"].ToString());
                this.controllingfaction = int.Parse(JZone["controlling-faction"].ToString());
                this.status = int.Parse(JZone["status"].ToString());
                this.next = long.Parse(JZone["next"].ToString());
            }
        }
        public string type { get; internal set; }
        public string population { get; internal set; }
        public bool queue { get; internal set; }
        public RealmZone wintergrasp { get; internal set; }
        [JsonProperty("tol-barad")]
        public RealmZone tolbarad { get; internal set; }
        public bool status { get; internal set; }
        public string name { get; internal set; }
        public string slug { get; internal set; }
        public string battlegroup { get; internal set; }
        public string locale { get; internal set; }
        public string timezone { get; internal set; }
        public List<string> connected_realms { get; internal set; }

        public Realm(JObject rawJson)
        {
            this.type = rawJson["type"].ToString();
            this.population = rawJson["population"].ToString();
            this.queue = bool.Parse(rawJson["queue"].ToString());
            this.wintergrasp = new RealmZone(JObject.Parse(rawJson["wintergrasp"].ToString()));
            this.tolbarad = new RealmZone(JObject.Parse(rawJson["tol-barad"].ToString()));
            this.status = bool.Parse(rawJson["status"].ToString());
            this.name = rawJson["name"].ToString();
            this.slug = rawJson["slug"].ToString();
            this.battlegroup = rawJson["battlegroup"].ToString();
            this.locale = rawJson["locale"].ToString();
            this.timezone = rawJson["timezone"].ToString();
            this.connected_realms = new List<string>();
            foreach (string JConnectedRealm in rawJson["connected_realm"])
                this.connected_realms.Add(JConnectedRealm);
        }
    }
}
