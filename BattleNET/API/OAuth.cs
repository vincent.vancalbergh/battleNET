﻿using BattleNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BattleNET.API
{
    public class OAuth
    {
        public event EventHandler<AuthCodeEventArgs> AuthCode;
        protected virtual void OnAuthCode(AuthCodeEventArgs e)
        {
            EventHandler<AuthCodeEventArgs> handler = AuthCode;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private BattleNETClient parent;

        private string api_url;
        private string locale;
        private string api_key;
        private string api_secret;
        private string user_agent;

        private string auth_code;
        private string access_token;

        public OAuth(BattleNETClient parent, string api_url, string locale, string api_key, string api_secret, string user_agent)
        {
            this.parent = parent;

            this.api_url = api_url;
            this.locale = locale;
            this.api_key = api_key;
            this.api_secret = api_secret;
            this.user_agent = user_agent;
        }

        public string getAuthCode()
        {
            return $"{api_url}oauth/authorize?client_id={api_key}&scope=wow.profile+sc2.profile&redirect_uri=https://localhost:15753/oauth2callback/&response_type=code&approval_prompt=auto";
        }

        public void getAccessToken(string code)
        {
            Request request = new Request(user_agent);
            WebHeaderCollection header = new WebHeaderCollection();
            header.Add("Authorization", $"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{api_key}:{api_secret}"))}");
            request.POST($"{api_url}oauth/token?redirect_uri=https://localhost:15753/oauth2callback/&scope=wow.profile+sc2.profile&grant_type=authorization_code&code={code}", null, header);
            System.IO.File.WriteAllText("token.txt", $"{request.url}{Environment.NewLine}{request.response}");
        }
    }

    public class AuthCodeEventArgs : EventArgs
    {
        public string auth_code { get; internal set; }
    }
}
