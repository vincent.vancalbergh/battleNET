# Compatible API-Endpoints

| API                | CATEGORY              | METHOD                     | URL                                    | Battle.NET Function                                                   |
| :----------------- | :-------------------- | :------------------------- | :------------------------------------- | :-------------------------------------------------------------------- |
| D3 Community API   | PROFILE API           | CAREER PROFILE             | /D3/PROFILE/:BATTLETAG/                | BattleNETClient.D3.getCareerProfile(BATTLETAG)                        |
| D3 Community API   | PROFILE API           | HERO PROFILE               | /D3/PROFILE/:BATTLETAG/HERO/:ID        | BattleNETClient.D3.getHeroProfile(BATTLETAG, ID)                      |
| D3 Community API   | DATA RESOURCES        | ITEM DATA                  | /D3/DATA/ITEM/:DATA                    | BattleNETClient.D3.getItemData(DATA)                                  |
| D3 Community API   | DATA RESOURCES        | FOLLOWER DATA              | /D3/DATA/FOLLOWER/:FOLLOWER            | BattleNETClient.D3.getFollowerData(FOLLOWER)                          |
| D3 Community API   | IMAGES                | ITEM IMAGE                 |                                        | BattleNETClient.D3.getItemImage(SIZE, ICON)                           |
| D3 Community API   | IMAGES                | SKILL IMAGE                |                                        | BattleNETClient.D3.getSkillImage(SIZE, ICON)                          |
| SC2 Community APIs | PROFILE API           | PROFILE                    | /SC2/PROFILE/:ID/:REGION/:NAME/        | BattleNETClient.SC2.getProfile(ID, REGION, NAME)                      |
| SC2 Community APIs | PROFILE API           | LADDERS                    | /SC2/PROFILE/:ID/:REGION/:NAME/LADDERS | BattleNETClient.SC2.getLadders(ID, REGION, NAME)                      |
| SC2 Community APIs | PROFILE API           | MATCH HISTORY              | /SC2/PROFILE/:ID/:REGION/:NAME/MATCHES | BattleNETClient.SC2.getMatches(ID, REGION, NAME)                      |
| SC2 Community APIs | LADDER API            | LADDER                     | /SC2/LADDER/:ID                        | BattleNETClient.SC2.getLadder(ID)                                     |
| SC2 Community APIs | DATA RESOURCES        | ACHIEVEMENTS               | /SC2/DATA/ACHIEVEMENTS                 | BattleNETClient.SC2.getAchievements()                                 |
| SC2 Community APIs | DATA RESOURCES        | REWARDS                    | /SC2/DATA/REWARDS                      | BattleNETClient.SC2.getRewards()                                      |
| WoW Community APIs | ACHIEVEMENT API       | ACHIEVEMENT                | /WOW/ACHIEVEMENT/:ID                   | BattleNETClient.WOW.getAchievement(ID)                                |
| WoW Community APIs | AUCTION API           | AUCTION DATA STATUS        | /WOW/AUCTION/DATA/:REALM               | BattleNETClient.WOW.getAuction(REALM)                                 |
| WoW Community APIs | BOSS API              | MASTER LIST                | /WOW/BOSS/                             | BattleNETClient.WOW.getBossMasterList()                               |
| WoW Community APIs | BOSS API              | BOSS                       | /WOW/BOSS/:BOSSID                      | BattleNETClient.WOW.getBoss(BOSSID)                                   |
| WoW Community APIs | CHALLENGE MODE API    | REALM LEADERBOARDS         | /WOW/CHALLENGE/:REALM                  | BattleNETClient.WOW.getRealmLeaderboards(REALM)                       |
| WoW Community APIs | CHALLENGE MODE API    | REGION LEADERBOARDS        | /WOW/CHALLENGE/REGION                  | BattleNETClient.WOW.getRegionLeaderboards()                           |
| WoW Community APIs | CHARACTER PROFILE API | CHARACTER PROFILE          | /WOW/CHARACTER/:REALM/:CHARACTERNAME   | BattleNETClient.WOW.getCharacterProfile(REALM, CHARACTERNAME)         |
| WoW Community APIs | CHARACTER PROFILE API | CHARACTER PROFILE + FIELDS | /WOW/CHARACTER/:REALM/:CHARACTERNAME   | BattleNETClient.WOW.getCharacterProfile(REALM, CHARACTERNAME, FIELDS) |
| WoW Community APIs | GUILD PROFILE API     | GUILD PROFILE              | /WOW/GUILD/:REALM/:GUILDNAME           | BattleNETClient.WOW.getGuildProfile(REALM, GUILDNAME)                 |
| WoW Community APIs | GUILD PROFILE API     | GUILD PROFILE + FIELDS     | /WOW/GUILD/:REALM/:GUILDNAME           | BattleNETClient.WOW.getGuildProfile(REALM, GUILDNAME, FIELDS)         |
| WoW Community APIs | ITEM API              | ITEM                       | /WOW/ITEM/:ITEMID                      | BattleNETClient.WOW.getItem(ITEMID)                                   |
| WoW Community APIs | ITEM API              | ITEM SET                   | /WOW/ITEM/SET/:SETID                   | BattleNETClient.WOW.getItemSet(SETID)                                 |
| WoW Community APIs | MOUNT API             | MASTER LIST                | /WOW/MOUNT/                            | BattleNETClient.WOW.getMountMasterList()                              |
| WoW Community APIs | PET API               | MASTER LIST                | /WOW/PET/                              | BattleNETClient.WOW.getPetMasterList()                                |
There is more. I will update this table soon